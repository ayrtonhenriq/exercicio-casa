package br.com.itau.investimentocliente.services;

import br.com.itau.investimentocliente.models.Cliente;
import br.com.itau.investimentocliente.repositories.ClienteRepository;
import br.com.itau.investimentocliente.services.ClienteService;
import spock.lang.Specification;
import java.util.Optional;

class ClienteServiceTest extends Specification {
    ClienteService clienteService;
    ClienteRepository clientRepository;
    
    def setup() {
        clienteService = new ClienteService()
        clientRepository = Mock()
        clienteService.clienteRepository = clientRepository
    }

    def 'Deve salvar um cliente na base'() {
        given: 'os dados devem ser preenchidos'
        Cliente cliente = new Cliente()
        cliente.setCpf('430718868637')
        cliente.setNome('Teste ayrton')

        when: 'cleinte  salvo'
        def clienteSalvo = clienteService.cadastrar(cliente)

        then: 'retorne um cliente'
        1 * clientRepository.save(_) >> cliente
        clienteSalvo != null
    }

    def 'deve buscar um cliente pelo CPF'(){
		given: 'os dados do cliente existem na base'
		String cpf = '123.123.123-12'
		Cliente cliente = new Cliente()		
		cliente.setNome('José')
		cliente.setCpf(cpf)
		def clienteOptional = Optional.of(cliente)
		
		when: 'é feito uma busca informando o cpf'
		def clienteEncontrado = clienteService.buscar(cpf)
		
		then: 'retorne o cliente buscado'
		1 * clientRepository.findByCpf(_) >> clienteOptional
		clienteEncontrado.isPresent() == true
	}

 }
